# Determinant

A basic Matrix client interface for Python.

### Supported Features

- Receiving messages
- Sending messages

## Example Usage

Async:
```py
import determinant
import secret

client = determinant.Client()
client.authenticate(secret.user, secret.password)

@client.handler
async def yay(event):
    if event.type == "m.room.message" and event.content and event.content.body:
        if event.content.body == "le ping":
            await event.room.send("le pong")

client.start()
```

Sync:
```py
import determinant
import secret

client = determinant.SyncClient()
client.authenticate(secret.user, secret.password)

@client.handler
def yay(event):
    if event.type == "m.room.message" and event.content and event.content.body:
        if event.content.body == "le ping":
            event.room.sync_send("le pong")

client.start()
```