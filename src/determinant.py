import protocol
import secret
import time
import objects
import asyncio
import aiohttp

class NotAuthenticatedException(Exception):
    pass

class SyncClient():
    def __init__(self, homeserver_endpoint="https://matrix.org/_matrix/client/v3", event_handlers = [], token = "", loop_delay = 0.5):
        self.homeserver_endpoint = homeserver_endpoint
        self.event_handlers = event_handlers
        self.token = token
        self.loop_delay = loop_delay
    
    def handler(self, func):
        self.event_handlers.append(func)
    
    def authenticate(self, user, password, endpoint="https://matrix.org/_matrix/client/r0/login"):
        self.token = protocol.sync_get_token(endpoint, secret.user, secret.password)
    
    def sync_send(self, room_id, body):
        if self.token == "":
            raise NotAuthenticatedException("The client has not authenticated with Matrix yet.")

        protocol.sync_send_message(self.homeserver_endpoint, room_id, self.token, body)
    
    def start(self):
        if self.token == "":
            raise NotAuthenticatedException("The client has not authenticated with Matrix yet.")

        user_state = protocol.sync_get_sync(self.homeserver_endpoint, self.token)

        self.next_batch = user_state["next_batch"]
        
        while True:

            batch = protocol.sync_get_sync(self.homeserver_endpoint, self.token, self.next_batch)
            self.next_batch = batch["next_batch"]
            if not ("rooms" in batch.keys()):
                continue
            
            rooms = batch["rooms"]["join"]

            for room_id in rooms.keys():
                room = rooms[room_id]
                events = room["timeline"]["events"]

                for raw_event in events:
                    event = objects.Event(self, raw_event, room_id)

                    for handler in self.event_handlers:
                        handler(event)
                
            
            time.sleep(self.loop_delay)

class Client():
    def __init__(self, homeserver_endpoint="https://matrix.org/_matrix/client/v3", event_handlers = [], token = "", loop_delay = 0.5):
        self.homeserver_endpoint = homeserver_endpoint
        self.event_handlers = event_handlers
        self.token = token
        self.loop_delay = loop_delay
    
    def handler(self, func):
        self.event_handlers.append(func)
    
    def authenticate(self, user, password, endpoint="https://matrix.org/_matrix/client/r0/login"):
        self.token = protocol.sync_get_token(endpoint, secret.user, secret.password)
    
    async def send(self, room_id, body):
        if self.token == "":
            raise NotAuthenticatedException("The client has not authenticated with Matrix yet.")

        await protocol.async_send_message(self.session, self.homeserver_endpoint, room_id, self.token, body)
    
    async def run(self):

        async with aiohttp.ClientSession() as session:
            
            self.session = session

            if self.token == "":
                raise NotAuthenticatedException("The client has not authenticated with Matrix yet.")

            user_state = await protocol.async_get_sync(self.session, self.homeserver_endpoint, self.token)

            self.next_batch = user_state["next_batch"]
            
            while True:

                batch = await protocol.async_get_sync(self.session, self.homeserver_endpoint, self.token, self.next_batch)
                self.next_batch = batch["next_batch"]
                if not ("rooms" in batch.keys()):
                    continue
                
                rooms = batch["rooms"]["join"]

                for room_id in rooms.keys():
                    room = rooms[room_id]
                    events = room["timeline"]["events"]

                    for raw_event in events:
                        event = objects.Event(self, raw_event, room_id)

                        for handler in self.event_handlers:
                            await handler(event)
                
                await asyncio.sleep(self.loop_delay)
    
    def start(self):

        asyncio.run(self.run())