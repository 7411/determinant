class Event():
    def __init__(self, client, event, room_id=None):
        self.id = event["event_id"]
        self.client = client
        self.json = event
        keys = event.keys()
        if "type" in keys:
            self.type = event["type"]
        
        if room_id:
            self.room_id = room_id
            self.room = Room(self.client, room_id)
        elif "room_id" in keys:
            self.room_id = event["room_id"]
            self.room = Room(self.client, room_id)
        else:
            self.room_id = None
            self.room = None
        
        if "content" in keys:
            self.content = EventContent(event["content"])
        else:
            self.content = None

class EventContent():
    def __init__(self, content):
        keys = content.keys()
        if "body" in keys:
            self.body = content["body"]
        else:
            self.body = None
        if "msgtype" in keys:
            self.msgtype = content["msgtype"]
        else:
            self.msgtype = None

class Room():
    def __init__(self, client, room_id):
        self.id = room_id
        self.client = client
    
    def sync_send(self, body):
        self.client.sync_send(self.id, body)
    
    async def send(self, body):
        await self.client.send(self.id, body)