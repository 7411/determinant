# API to construct requests for usage with the Matrix protocol.

import json
import time
import requests

### GET TOKEN

def _post_get_token_format(api, user, password):
    return (api,
        json.dumps({
            "type" : "m.login.password",
            "identifier" : {
                "type" : "m.id.user",
                "user" : user
            },
            "password" : password
        })
    )

def sync_get_token(api, user, password):
    url, body = _post_get_token_format(api, user, password)
    return requests.post(
        url,
        data = body,
        headers = {"Content-Type" : "application/json"}
    ).json()["access_token"]

### SEND MESSAGE

def _put_send_message_format(api, room_id, token, body):
    return (f"{api}/rooms/{room_id}/send/m.room.message/{int(time.time()*1000)}?access_token={token}",
        json.dumps({
            "msgtype" : "m.text",
            "body" : body
        })
    )

def sync_send_message(api, room_id, token, body):
    url, body = _put_send_message_format(api, room_id, token, body)
    return requests.put(
        url,
        data = body,
        headers = {"Content-Type" : "application/json"}
    ).json()

async def async_send_message(session, api, room_id, token, body):
    url, body = _put_send_message_format(api, room_id, token, body)
    async with session.put(
        url,
        data = body,
        headers = {"Content-Type" : "application/json"}
    ) as response:
        return (await response.json())

### GET EVENTS SURROUNDING EVENT

def _get_surrounding_events_format(api, room_id, token, event_id):
    return f"{api}/rooms/{room_id}/context/{event_id}?access_token={token}&limit=10"

def sync_get_surrounding_events(api, room_id, token, event_id):
    url = _get_surrounding_events_format(api, room_id, token, event_id)
    return requests.get(
        url,
        headers = {"Content-Type" : "application/json"}
    ).json()

async def async_get_surrounding_events(session, api, room_id, token, event_id):
    url = _get_surrounding_events_format(api, room_id, token, event_id)
    async with session.get(
        url,
        headers = {"Content-Type" : "application/json"}
    ) as response:
        return (await response.json())

### INITIAL SYNC USER STATE

def _get_sync_format(api, token, since=None):
    since = f"&since={since}" if since else ""
    return f"{api}/sync?access_token={token}{since}"

def sync_get_sync(api, token, since=None):
    url = _get_sync_format(api, token, since)
    return requests.get(
        url,
        headers = {"Content-Type" : "application/json"}
    ).json()

async def async_get_sync(session, api, token, since=None):
    url = _get_sync_format(api, token, since)
    async with session.get(
        url,
        headers = {"Content-Type" : "application/json"}
    ) as response:
        return (await response.json())
